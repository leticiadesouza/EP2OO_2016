package view;
import java.awt.FlowLayout;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBuffer;
import java.io.BufferedReader;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Scanner;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class PGMreader {

	public static String le_linha(FileInputStream arquivo) {
		String linha = "";
		byte bb;
		try {
			while ((bb = (byte) arquivo.read()) != '\n') {
				linha += (char) bb;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Linha: " + linha);
		return linha;
	}
	public static void main(String args[]) throws Exception {
		try {
			FileInputStream fileInputStream2 = new FileInputStream("lena.pgm");
			BufferedImage imagem_pgm2 = null, imagem_pgm_negativo = null;
		    int width1 = 0;
		    int height1 = 0;
		    int maxVal1 = 0;
			int count = 0;
			byte bb;
			
			String linha = PGMreader.le_linha(fileInputStream2);
			if("P5".equals(linha)) {
				linha = PGMreader.le_linha(fileInputStream2);
				while (linha.startsWith("#")) {
					linha = PGMreader.le_linha(fileInputStream2);
				}
			    Scanner in = new Scanner(linha); 
			    if(in.hasNext() && in.hasNextInt())
			    	width1 = in.nextInt();
			    else
			    	System.out.println("Arquivo corrompido");
			    if(in.hasNext() && in.hasNextInt())
			    	height1 = in.nextInt();
			    else
			    	System.out.println("Arquivo corrompido");
				linha = PGMreader.le_linha(fileInputStream2);
				in.close();
				in = new Scanner(linha);
				maxVal1 = in.nextInt();
				in.close();
				
				imagem_pgm2 = new BufferedImage(width1, height1, BufferedImage.TYPE_BYTE_GRAY);
				imagem_pgm_negativo = new BufferedImage(width1, height1, BufferedImage.TYPE_BYTE_GRAY);
				byte [] pixels2 = ((DataBufferByte) imagem_pgm2.getRaster().getDataBuffer()).getData();
				byte [] pixels_negativo = ((DataBufferByte) imagem_pgm_negativo.getRaster().getDataBuffer()).getData();

				System.out.println("Arquivo1 - Height: " + height1 + " Width: " + width1 + " Max: " + maxVal1);
				
				while(count < (height1*width1)) {
					bb = (byte) fileInputStream2.read();
					pixels2[count] = bb;
					pixels_negativo[count] = (byte) (maxVal1 - bb); 
					count++;
				}				
			}
			else {
				System.out.println("Arquivo inválido");
			}
			
			System.out.println("Height=" + height1);
			System.out.println("Width=" + width1);
			System.out.println("Total de Pixels = " + (width1 * height1));
			System.out.println("Total de Pixels lidos = " + count);

			JFrame frame = new JFrame();
			frame.getContentPane().setLayout(new FlowLayout());
			frame.getContentPane().add(new JLabel(new ImageIcon(imagem_pgm2)));
			frame.getContentPane().add(new JLabel(new ImageIcon(imagem_pgm_negativo)));
			frame.pack();
			frame.setVisible(true);
			fileInputStream2.close();
		}
		catch(Throwable t) {
			t.printStackTrace(System.err) ;
			return ;
		}


	}
}

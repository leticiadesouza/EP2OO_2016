package Controller;

public class decifraImagem {
	
	protected int largura;
	protected int altura;
	protected String numeroMagico;
	protected int maxColor;
	protected String caminhoDaImagem;
	
	public void setLargura (int largura) {
		this.largura = largura;
	}
	public int getLargura() {
		return largura;
	}
	public void setAltura (int altura) {
		this.altura = altura;
	}
	public int getAltura (int altura) {
		return altura;
	}
	public void setNumeroMagico (String numeroMagico) {
		this.numeroMagico = numeroMagico;
	}
	public String getNumeroMagico (String numeroMagico) {
		return numeroMagico;
	}
	public void setMaxColor (int maxColor) {
		this.maxColor = maxColor;
	}
	public int getMaxColor (int maxColor) {
		return maxColor;
	}
	public void setCaminhoDaImagem (String caminhoDaImagem) {
		this.caminhoDaImagem = caminhoDaImagem;
	}
	public String getCaminhoDaImagem (String caminhoDaImagem) {
		return caminhoDaImagem;
	}
	

}

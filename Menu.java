package view;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


public class Menu extends JFrame{

	private JPanel painelMensagem;
	private JButton btn1;
	private JButton btn2;
	
	public Menu(BufferedImage image_pgm2, BufferedImage negative){
		super("Menu");
		criaMenu(image_pgm2, negative);
	}
	
	public void criaMenu(final BufferedImage image_pgm2, final BufferedImage negative){
		getContentPane().setLayout(new FlowLayout());
		setSize(500, 500);
		painelMensagem = new JPanel();
		painelMensagem.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(painelMensagem);
		
		JLabel mensagem = new JLabel("Selecione a imagem que deseja ver: ");
		painelMensagem.add(mensagem);

		btn1 = new JButton("Imagem 1");
		btn2 = new JButton("Imagem 2");
		
		btn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFrame frame = new JFrame();
				frame.getContentPane().add(new JLabel(new ImageIcon(image_pgm2)));
				frame.setVisible(true);
				frame.pack();
			}
		});
		
		btn2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFrame frame = new JFrame();
				frame.getContentPane().add(new JLabel(new ImageIcon(negative)));
				frame.setVisible(true);
				frame.pack();
			}
		});
		
		painelMensagem.add(btn1);
		painelMensagem.add(btn2);
	}

}
